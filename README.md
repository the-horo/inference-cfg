<h1>Configuration</h1>
<p>Inference personal configuration files.</p>
<br>
<p>These configuration files are targeting Linux, Gentoo Hardened,<br>
musl libc, and LLVM toolchain. Anything else is out of scope and<br>
unsupported. I am working on moving all GCC-related configurations to LLVM,<br>
and GCC will eventually be dropped, entirely. They also focus on hardening<br>
the system as much as possible, without making it unusable; expect<br>
noticeable performance impact, although the system should still be usable.<br>
Security and privacy are the focuses, nothing else.
<br>
<br>
<h2>Licensing</h2>
<p>All content is licensed under MIT License.</p>
<br>
<br>
<h2>Security</h2>
<p>All files are checked for security issues; however, it is always the<br>
user's responsibility to audit the code before installing and/or executing<br>
it.<br>
<br>
The maintainers take no responsibility for any security issues which<br>
may arise due to usage of this repository.</p>
<br>
<br>
<h2>Policy</h2>
<h3>Contributing</h3>
<p>To contribute to the project, submit a pull request. All pull requests<br>
are subject to approval.<br>
<br>
Code must follow the standard style for Inferencium Network code:<br>
- 80 character column limit.<br>
- Tab indents.<br>
- 1 tab indent equals 4 columns.<br>
- Comments must be used for each different block of code.<br>
- Comments must be concise. Do not make comments longer than necessary.<br>
- Code and comments must be professional. No funny or vulgar code or<br>
comments.<br>
<br>
Files must be stored in their related directories.<br>
<br>
Text files must be stored as plain text (.txt) or Markdown (.md) files.<br>
<br>
All dates and times must be ISO 8601 compliant (YYYY-MM-DD HH:MM).<br>
<br>
All filenames must be most significant to least significant, lowercase, and<br>
hyphens must be used instead of spaces.</p>
<br>
<br>
<h2>Branches</h2>
<h3>dev</h3>
<p>Development branch. All pre-alpha development and alpha testing happens<br/>
here.<br/>
Merge requests must be made to the dev branch.</p>
<br>
<h3>beta</h3>
<p>Beta branch. All beta testing of the software happens here.</p>
<br>
<h3>stable</h3>
<p>Stable branch. Complete and stable versions of the software are stored<br/>
here.<br/>
<br/>
