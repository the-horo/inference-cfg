# Portage make.conf

# Copyright 2022 Inference


# Toolchain.
AR="llvm-ar"
CC="clang"
CPP="clang-cpp"
CXX="clang++"
LD="ld.lld"
NM="llvm-nm"
OBJCOPY="llvm-objcopy"
OBJDUMP="llvm-objdump"
RANLIB="llvm-ranlib"
READELF="llvm-readelf"
STRINGS="llvm-strings"
STRIP="llvm-strip"


# Flags.
## Hardening flags.
C_SEC="-fPIE -fPIC -fstack-protector-all -fstack-clash-protection -D_FORTIFY_SOURCE=2 -ftrivial-auto-var-init=zero -enable-trivial-auto-var-init-zero-knowing-it-will-be-removed-from-clang -fwrapv"
LD_SEC="-Wl,-pie -Wl,--strip-all -Wl,-z,defs -Wl,-z,now -Wl,-z,relro"

## Compiler flags.
CFLAGS="-march=znver3 -O2 -pipe -flto=thin -U__gnu_linux__ ${C_SEC}"
CXXFLAGS="-march=znver3 -O2 -pipe -flto=thin ${C_SEC}"

## Linker flags.
LDFLAGS="-fuse-ld=lld -rtlib=compiler-rt -unwindlib=libunwind -Wl,--thinlto-jobs=4 ${LD_SEC}"

## USE flags.
USE="clang dbus elogind libcxx libedit llvm-libunwind lto nftables pulseaudio system-av1 system-harfbuzz system-icu system-jpeg system-libvpx system-llvm system-png system-webp verify-sig wayland"
USE="${USE} -ipv6 -jit -systemd"

## CPU flags.
CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext pclmul popcnt rdrand sha sse sse2 sse3 sse4_1 sse4_2 sse4a ssse3"

## Video card flags.
VIDEO_CARDS="amdgpu radeonsi"

## ABI flags.
ABI_X86="64"

## LLVM target flags.
LLVM_TARGETS="X86"


# System.
## CHOST.
CHOST="x86_64-gentoo-linux-musl"

## Directories.
PORTDIR="/var/db/repos/gentoo"
DISTDIR="/var/cache/distfiles"
PKGDIR="/var/cache/binpkgs"

## Language.
LC_MESSAGES=C

## Gentoo mirrors.
GENTOO_MIRRORS="https://mirror.init7.net/gentoo/"

## Emerge.
EMERGE_DEFAULT_OPTS="--ask --verbose"
FEATURES="buildpkg"
MAKEOPTS="-j4"
