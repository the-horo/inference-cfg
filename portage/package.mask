# Portage package.mask

# Copyright 2022 Inference


# Global.
#*/*x11*
#*/*xorg*
app-admin/sudo
gui-wm/hikari
sys-libs/libunwind
www-client/firefox

# Gentoo repository.
app-office/libreoffice::gentoo
dev-java/icedtea-bin::gentoo
dev-java/icedtea::gentoo
dev-java/openjdk::gentoo
dev-lang/rust-bin::gentoo
dev-lang/rust::gentoo
dev-python/pypy3-exe::gentoo
dev-util/dwarves::gentoo
dev-util/kbuild::gentoo
dev-util/valgrind::gentoo
media-libs/mesa::gentoo
media-libs/zvbi::gentoo
media-sound/sox::gentoo
media-tv/kodi::gentoo
media-video/guvcview::gentoo
net-dialup/linux-atm::gentoo
net-fs/cifs-utils::gentoo
net-fs/nfs-utils::gentoo
net-fs/samba::gentoo
net-libs/libnfsidmap::gentoo
net-misc/connman::gentoo
net-misc/spice-gtk::gentoo
net-wireless/bluez::gentoo
sys-apps/accountsservice::gentoo
sys-apps/bubblewrap::gentoo
sys-apps/fakeroot::gentoo
sys-apps/intel-sa-00075-tools::gentoo
sys-apps/isapnptools::gentoo
sys-apps/iucode_tool::gentoo
sys-apps/tcp-wrappers::gentoo
sys-apps/watchdog::gentoo
sys-apps/xdg-dbus-proxy::gentoo
#sys-auth/elogind::gentoo
sys-auth/nss-myhostname::gentoo
sys-auth/polkit::gentoo
sys-block/partimage::gentoo
sys-block/thin-provisioning-tools::gentoo
sys-boot/lilo::gentoo
sys-boot/syslinux::gentoo
sys-devel/gdb::gentoo
sys-fs/lvm2::gentoo
sys-fs/lvm2::gentoo
sys-fs/reiserfsprogs::gentoo
sys-libs/tevent::gentoo
x11-apps/igt-gpu-tools::gentoo
x11-apps/sessreg::gentoo
x11-base/xorg-server::gentoo
x11-libs/libva-vdpau-driver::gentoo
x11-libs/vte::gentoo
x11-wm/fluxbox::gentoo
